﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var theQueue = new BlockingCollection<EmailModel>(new ConcurrentQueue<EmailModel>());
            var primaryWork = new PrimaryWork(theQueue);
            var mailSender = new EmailSender(theQueue);
            mailSender.Start();
            primaryWork.DoWork();
            // Wait for all mails to be sent (could also use a Cancellation Token, if we wanted to quit without sending)
            await mailSender.Stop();
        }
    }

    public class PrimaryWork
    {
        private readonly BlockingCollection<EmailModel> _enqueuer;

        public PrimaryWork(BlockingCollection<EmailModel> enqueuer)
        {
            _enqueuer = enqueuer;
        }

        public void DoWork()
        {
            // ... do your work
            for (var i = 0; i < 100; i++)
            {
                EnqueueEmail(new EmailModel { To = $"recipient{i}@foo.com", Message = $"Message {i}" });
            }
        }

        private void EnqueueEmail(EmailModel message)
        {
            _enqueuer.Add(message);
        }
    }

    public class EmailModel
    {
        public string To { get; set; }
        public string Message { get; set; }
    }

    public class EmailSender
    {
        private readonly BlockingCollection<EmailModel> _mailQueue;
        private readonly TaskCompletionSource<string> _tcsIsCompleted = new TaskCompletionSource<string>();

        public EmailSender(BlockingCollection<EmailModel> mailQueue)
        {
            _mailQueue = mailQueue;
        }

        public void Start()
        {
            Task.Run(() =>
            {
                try
                {
                    while (!_mailQueue.IsCompleted)
                    {
                        var nextMessage = _mailQueue.Take();
                        SendEmail(nextMessage).Wait();
                    }
                    _tcsIsCompleted.SetResult("ok");
                }
                catch (Exception)
                {
                    _tcsIsCompleted.SetResult("fail");
                }
            });
        }

        public async Task Stop()
        {
            _mailQueue.CompleteAdding();
            await _tcsIsCompleted.Task;
        }

        private async Task SendEmail(EmailModel message)
        {
            // IO Bound work goes here ...
            // Ideally, try and keep long lived 
            // Also, it may be able to bulk send mails?
            Console.WriteLine($"Send mail message {message.Message} to {message.To}");
        }
    }
}
